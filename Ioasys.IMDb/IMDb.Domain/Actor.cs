﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IMDb.Domain
{
    public class Actor
    {
        public int Id { get; set; }
        public String Name { get; set; }
    }
}

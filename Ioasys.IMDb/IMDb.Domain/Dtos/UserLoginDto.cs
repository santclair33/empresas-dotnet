using System;

namespace IMDb.Domain
{
    public class UserLoginDto
    {
        public String UserName { get; set; }
        public String Password { get; set; }
    }
}
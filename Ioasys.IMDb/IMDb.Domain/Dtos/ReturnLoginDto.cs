using System;

namespace IMDb.Domain
{
    public class ReturnLoginDto
    {
        public String Name { get; set; }
        public String UserName { get; set; }
        public String Email { get; set; }
        public String Type { get; set; }
    }
}
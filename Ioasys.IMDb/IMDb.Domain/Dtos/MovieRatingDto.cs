using System;
using System.Collections.Generic;

namespace IMDb.Domain
{
    public class MovieRatingDto
    {
        public int MovieId { get; set; }
        public int Assessment { get; set; }
    }
}
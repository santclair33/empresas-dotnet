using System;
using IMDb.Domain.Enum;

namespace IMDb.Domain
{
    public class UserDto
    {
        public int Id { get; set; }
        public String Name { get; set; }
        public String UserName { get; set; }
        public String Email { get; set; }
        public String Password { get; set; }
        public String Type { get; set; }
        public SituationEnum Situation { get; set; }
    }
}
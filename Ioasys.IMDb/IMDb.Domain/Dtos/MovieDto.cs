using System;
using System.Collections.Generic;

namespace IMDb.Domain
{
    public class MovieDto
    {
        public int Id { get; set; }
        public String Name { get; set; }
        public int Year { get; set; }
        public String Director { get; set; }
        public String Gender { get; set; }
        public List<ActorDto> Actors { get; set; }
        public Double AssessmentRating { get; set; }
        // public void getActors (int actorId, String actorName) {
        //     this.Actors.Add(
        //         new ActorDto(actorId, actorName)
        //     );
        // }
    }
}
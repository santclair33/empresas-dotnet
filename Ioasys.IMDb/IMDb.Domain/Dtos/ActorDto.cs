using System;

namespace IMDb.Domain
{
    public class ActorDto
    {
        public int Id { get; set; }
        public String Name { get; set; }
        // public ActorDto(int Id, String Name)
        // {
        //     this.Id = Id;
        //     this.Name = Name;
        // }
    }
}
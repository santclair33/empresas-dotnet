﻿using System;

namespace IMDb.Domain
{
    public class MovieRating
    {
        public int Id { get; set; }
        public int MovieId { get; set; }
        public int UserId { get; set; }
        public int Assessment { get; set; }
    }
}

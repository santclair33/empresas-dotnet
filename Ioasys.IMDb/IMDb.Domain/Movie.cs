﻿using System;
using System.Collections.Generic;

namespace IMDb.Domain
{
    public class Movie
    {
        public int Id { get; set; }
        public String Name { get; set; }
        public int Year { get; set; }
        public String Director { get; set; }
        public String Gender { get; set; }
        public List<Actor> Actors { get; set; }

    }
}
﻿using IMDb.Domain.Enum;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace IMDb.Domain
{
    public class User : IdentityUser<int>
    {
        [Column(TypeName = "nvarchar(150)")]
        public String Name { get; set; }
        public String Password { get; set; }
        public String Type { get; set; }
        public SituationEnum Situation { get; set; }
        public List<UserRole> UserRoles { get; set; }
    }
}

﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

using IMDb.Domain;
using System;
using System.Collections.Generic;

namespace IMDb.Repository
{
    public class IMDbRepository : IIMDbRepository
    {
        private readonly IMDbContext _context;
        public IMDbRepository(IMDbContext context)
        {
            _context = context;
            _context.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;
        }
        public void Add<T>(T entity) where T : class
        {
            _context.Add(entity);
        }
        public void Update<T>(T entity) where T : class
        {
            _context.Update(entity);
        }
        public void Delete<T>(T entity) where T : class
        {
            _context.Remove(entity);
        }
        public async Task<bool> SaveChangesAsync()
        {
            return (await _context.SaveChangesAsync()) > 0;
        }

        public async Task<Movie[]> GetAllMoviesAsync()
        {
            IQueryable<Movie> query = _context.Movies;

            return await query.ToArrayAsync();
        }
        public async Task<User[]> GetAllUserAsync()
        {
            IQueryable<User> query = _context.Users
                .Where(a => a.Type == "public")
                .OrderBy(u => u.UserName);

            return await query.ToArrayAsync();
        }
        public async Task<Actor> GetActorAsync(string actorName)
        {
           IQueryable<Actor> query = _context.Actors;
           query = query.Where(a => a.Name == actorName);

            return await query.FirstOrDefaultAsync();
        }
        public async Task<Actor> GetActorByIdAsync(int actorId)
        {
            IQueryable<Actor> query = _context.Actors.Where(a => a.Id == actorId);

            return await query.FirstOrDefaultAsync();
        }
        public async Task<User> GetUserByUserName(string userName)
        {
            IQueryable<User> query = _context.Users;
            query = query.Where(u => u.UserName == userName);

            return await query.FirstOrDefaultAsync();
        }
        public async Task<User> GetUserById(int userId)
        {
            IQueryable<User> query = _context.Users;
            query = query.Where(u => u.Id == userId);

            return await query.FirstOrDefaultAsync();
        }
        public async Task<MovieActor[]> GetMovieActorByMovieIdAsync(int movieId)
        {
            IQueryable<MovieActor> query = _context.MovieActors
                .Where(ma => ma.MovieId == movieId);
           return await query.ToArrayAsync();
        }
        public async Task<MovieRating[]> GetMovieRatingsByMovieIdAsync(int movieId)
        {
            IQueryable<MovieRating> query = _context.MovieRatings
                .Where(ma => ma.MovieId == movieId);
            return await query.ToArrayAsync();
        }
    }
}

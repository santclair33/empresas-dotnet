﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using IMDb.Domain;
namespace IMDb.Repository
{
    public interface IIMDbRepository
    {
        void Add<T>(T entity) where T : class;
        void Update<T>(T entity) where T : class;
        void Delete<T>(T entity) where T : class;
        Task<bool> SaveChangesAsync();
        Task<Movie[]> GetAllMoviesAsync();
        Task<User[]> GetAllUserAsync();
        Task<Actor> GetActorAsync(String actorName);
        Task<Actor> GetActorByIdAsync(int actorId);
        Task<User> GetUserByUserName(String userName);
        Task<User> GetUserById(int userId);
        Task<MovieActor[]> GetMovieActorByMovieIdAsync(int movieId);
        Task<MovieRating[]> GetMovieRatingsByMovieIdAsync(int movieid);

    }
}

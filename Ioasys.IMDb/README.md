# Desafio Pessoa Desenvolvedora .NET
### Sant Clair
[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger)

- Asp.Net Core (.netcore 3.1)
- Entity Framework Core
- Sql Server

Uma migration está criada, abaixo os comandos que podem ser executados no Package Manager Console para criação do bando de dados
Obs: Foi utilizado uma esrtutura de camadas, os comandos abaixo devem ser executados no IMDb.Repository.
```sh
remove-migration (caso queira gerar uma nova migragtion)
add-migration init
update-database
```
O swagger é disparado automaticamente ao executar a aplicação, por padrão nos endereços:
- https://localhost:5001/swagger/index.html
- https://localhost:44306/swagger/index.html

Qualquer dúvida ou necessitando de mais informações, estou à disposição.
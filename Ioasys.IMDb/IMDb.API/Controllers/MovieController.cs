﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using IMDb.Domain;
using IMDb.Repository;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace IMDb.API.Controllers
{
    /// <summary> Controller dos catalogos de títulos </summary>
    [ApiController]
    [Route("api/[controller]")]
    public class MovieController : ControllerBase
    {
        private readonly IIMDbRepository _repository;
        private readonly IMapper _mapper;
        /// <summary> Injeção de dependência </summary>
        public MovieController(IIMDbRepository repository, IMapper mapper) {
            _repository = repository;
            _mapper = mapper;
        }
        /// <summary>
        /// Obter todos os filmes com sua informações e respectivas avaliações.
        /// </summary>
        /// <response code="200">A lista de filmes foi obtida com sucesso.</response>
        /// <response code="500">Ocorreu um erro ao obter a lista de filmes.</response>
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            try
            {
                return Ok(await GetMovieRating(await getActors(await _repository.GetAllMoviesAsync())) );
            }
            catch (System.Exception e )
            {
                Console.WriteLine(e);
                return this.StatusCode(StatusCodes.Status500InternalServerError, "Oops, algum erro aconteceu, entre em contato com o administrador do sistema. 1");
            }
        }
        /// <summary>
        /// Realizar cadastro de um novo filme.
        /// </summary>
        /// <response code="200">Cadastro realizado com sucesso.</response>
        /// <response code="500">Erro ao realizar cadastro.</response>
        [HttpPost]
        public async Task<IActionResult> Post(MovieDto movie)
        {
            try
            {
                var user = await _repository.GetUserByUserName(User.Identity.Name);
                if (user.Type == "admin")
                {
                    var model = _mapper.Map<Movie>(movie);
                    _repository.Add(model);

                    if (await _repository.SaveChangesAsync())
                    {
                        await AddActors(movie.Actors, model.Id);
                        return Created($"/api/movie/{model.Id}", model);
                    }
                } else {
                    return this.StatusCode(StatusCodes.Status406NotAcceptable, "Apenas usuario administrador pode cadatrar filmes.");
                }

            }
            catch (System.Exception)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, "Oops, algum erro aconteceu, entre em contato com o administrador do sistema. 10");
            }
            return BadRequest("");
        }
        private async Task AddActors(List<ActorDto> actors, int movieId)
        {
            foreach (var actor in actors)
            {
                var result = await _repository.GetActorAsync(actor.Name);

                if (result == null)
                {
                    var actorAdd = _mapper.Map<Actor>(actor);
                    _repository.Add(actorAdd);
                    await _repository.SaveChangesAsync();
                    Console.WriteLine(actorAdd.Id);
                    await AddMovieActor(actorAdd.Id, movieId);

                } else {
                    await AddMovieActor(result.Id, movieId);
                }

            }
        }
        private async Task AddMovieActor(int actorId, int movieId)
        {
            var movieActorAdd = new MovieActor{
                MovieId = movieId,
                ActorId = actorId
            };
            _repository.Add(movieActorAdd);
            await _repository.SaveChangesAsync();
        }
        private async Task<MovieDto[]> getActors(Movie[] movies)
        {
            MovieActor[] actors;
            var movieDto = _mapper.Map<MovieDto[]>(movies);

                int index = 0;
            foreach (var item in movies)
            {
                actors = await _repository.GetMovieActorByMovieIdAsync(item.Id);
                movieDto[index] =  _mapper.Map<MovieDto>(item);
                foreach (var i in actors)
                {
                    var actor = await _repository.GetActorByIdAsync(i.ActorId);

                    var newActor  = new ActorDto();
                    newActor.Id = i.ActorId;
                    newActor.Name = actor.Name;
                    movieDto[index].Actors.Add(newActor);
                }
                index++;
            }
            return movieDto;
        }
        private async Task<MovieDto[]> GetMovieRating(MovieDto[] movies)
        {
            int index = 0;
            foreach (var item in movies)
            {
                var movieRating = await _repository.GetMovieRatingsByMovieIdAsync(item.Id);
                foreach (var i in movieRating)
                {
                    if (i.MovieId == item.Id) {
                        Double averageListPrice = movieRating.Average(mr => mr.Assessment);
                        movies[index].AssessmentRating = averageListPrice;
                    }
                }
                index++;
            }
            return movies;
        }
    }
}

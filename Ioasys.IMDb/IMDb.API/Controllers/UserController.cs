using System.Threading.Tasks;
using IMDb.Domain;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Security.Claims;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using System;
using System.IdentityModel.Tokens.Jwt;
using IMDb.Repository;

namespace IMDb.API.Controllers
{
    /// <summary> Controller de usuario e autenticação. </summary>
    [ApiController]
    [Route("api/[controller]")]
    public class UserController : ControllerBase
    {
        private readonly IConfiguration _config;
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;
        private readonly IMapper _mapper;
        private readonly IIMDbRepository _repository;
        /// <summary> Injeção de dependência </summary>
        public UserController(IConfiguration config, UserManager<User> userManager, SignInManager<User> signInManager, IMapper mapper, IIMDbRepository repository)
        {
           _config = config;
           _userManager = userManager;
           _signInManager = signInManager;
           _mapper = mapper;
           _repository = repository;
        }
        /// <summary>
        /// Obter todos os usuários que não são administradores.
        /// </summary>
        /// <response code="200">A lista de usuários foi obtida com sucesso.</response>
        /// <response code="500">Ocorreu um erro ao obter a lista de usuários.</response>
        [HttpGet("GetUser")]
        public async Task<IActionResult> GetUser()
        {
            var userDto = _mapper.Map<ReturnLoginDto[]>(await _repository.GetAllUserAsync());

            return Ok(userDto);
        }
        /// <summary>
        /// Cadastrar um novo usuário.
        /// </summary>
        /// <response code="200">Usuário cadastrado com sucesso.</response>
        /// <response code="500">Ocorreu um erro ao cadastrar o usuário.</response>
        [HttpPost("Register")]
        [AllowAnonymous]
        public async Task<IActionResult> Register(UserDto userDto)
        {
            try
            {
                var user = _mapper.Map<User>(userDto);

                var result = await _userManager.CreateAsync(user ,userDto.Password);

                var userToReturn = _mapper.Map<ReturnLoginDto>(user);

                if (result.Succeeded)
                {
                    return Created("GetUser", userToReturn);
                }

                return BadRequest(result.Errors);
            }
            catch (System.Exception)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, "Oops, algum erro aconteceu, entre em contato com o administrador do sistema. 7");;
            }
        }
        /// <summary>
        /// Login do usuario
        /// </summary>
        /// <response code="200">Login realizado com sucesso.</response>
        /// <response code="500">Ocorreu um erro ao realizar login.</response>
        [HttpPost("Login")]
        [AllowAnonymous]
        public async Task<IActionResult> Login(UserLoginDto userLogin)
        {
            try
            {
                var user = await _userManager.FindByNameAsync(userLogin.UserName);

                var result = await _signInManager.CheckPasswordSignInAsync(user, userLogin.Password, false);

                if (result.Succeeded)
                {
                    var appUser = await _userManager.Users
                        .FirstOrDefaultAsync(u => u.NormalizedUserName == userLogin.UserName.ToUpper());

                    var userToReturn = _mapper.Map<UserLoginDto>(appUser);

                    return Ok(
                        new {
                            token = GenerateJWT(appUser).Result,
                            user = userToReturn.UserName
                        }
                    );
                }

                return Unauthorized();
            }
            catch (System.Exception)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, "Oops, algum erro aconteceu, entre em contato com o administrador do sistema. 8");
            }
        }
        private async Task<string> GenerateJWT(User user)
        {
            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()),
                new Claim(ClaimTypes.Name, user.UserName)
            };

            var roles = await _userManager.GetRolesAsync(user);

            foreach (var role in roles)
            {
                claims.Add(new Claim(ClaimTypes.Role, role));
            }

            var key = new SymmetricSecurityKey(Encoding.ASCII
                .GetBytes(_config.GetSection("AppSettings:Token").Value));

            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha512Signature);

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(claims),
                Expires = DateTime.Now.AddDays(1),
                SigningCredentials = creds
            };

            var tokenHandler = new JwtSecurityTokenHandler();
            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }
        /// <summary>
        /// Editar usuário
        /// </summary>
        /// <response code="200">Usuário editado com sucesso.</response>
        /// <response code="500">Ocorreu um erro ao editar o usuário.</response>
        [HttpPut]
        public async Task<IActionResult> Put(int UserId, UserUpdateDto model)
        {
            try
            {
                var user = await _repository.GetUserById(UserId);
                if (user == null)
                {
                    return NotFound();
                }
                _repository.Update(model);

                if (await _repository.SaveChangesAsync())
                {
                    return Created($"/api/user/{model.Id}", model);
                }
            }
            catch (System.Exception)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, "Oops, algum erro aconteceu, entre em contato com o administrador do sistema. 9");
            }
            return BadRequest("");
        }
        /// <summary>
        /// Deletar usuário
        /// </summary>
        /// <response code="200">Usuário deletado com sucesso.</response>
        /// <response code="500">Ocorreu um erro ao deletar o usuário.</response>
        [HttpDelete]
        public async Task<IActionResult> Delete(int UserId, UserDto model)
        {
            try
            {
                var user = await _repository.GetUserById(UserId);
                if (user == null)
                {
                    return NotFound();
                }
                model.Situation = Domain.Enum.SituationEnum.Inactive;
                _repository.Update(model);

                if (await _repository.SaveChangesAsync())
                {
                    return Created($"/api/user/{model.Id}", model);
                }
            }
            catch (System.Exception)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, "Oops, algum erro aconteceu, entre em contato com o administrador do sistema. 10");
            }
            return BadRequest("");
        }
    }
}
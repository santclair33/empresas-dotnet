﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using IMDb.Domain;
using IMDb.Repository;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace IMDb.API.Controllers
{
    /// <summary> Controller dos catalogos de títulos </summary>
    [ApiController]
    [Route("api/[controller]")]
    public class MovieRatingController : ControllerBase
    {
        private readonly IIMDbRepository _repository;
        private readonly IMapper _mapper;
        /// <summary> Injeção de dependência </summary>
        public MovieRatingController(IIMDbRepository repository, IMapper mapper) {
            _repository = repository;
            _mapper = mapper;
        }
        /// <summary>
        /// Realizar avaliação de um título.
        /// </summary>
        /// <response code="200">Avaliação realizada com sucesso.</response>
        /// <response code="500">Erro ao realizar avaliação.</response>
        [HttpPost]
        public async Task<IActionResult> Post(MovieRatingDto movieRating)
        {
            try
            {
                var user = await _repository.GetUserByUserName(User.Identity.Name);
                if (user.Type == "public")
                {
                    var model = _mapper.Map<MovieRating>(movieRating);
                    if (model.Assessment > 4)
                    {
                        model.Assessment = 4;
                    }
                    model.UserId = user.Id;
                    _repository.Add(model);

                    if (await _repository.SaveChangesAsync())
                    {
                        return Created($"/api/movierating/{model.Id}", _mapper.Map<MovieRatingDto>(model));
                    }
                } else {
                    return this.StatusCode(StatusCodes.Status406NotAcceptable, "Apenas usuários podem votar nos filmes.");
                }
            }
            catch (System.Exception)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, "Oops, algum erro aconteceu, entre em contato com o administrador do sistema. 5");
            }
            return BadRequest("");
        }
    }
}

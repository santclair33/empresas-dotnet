﻿using AutoMapper;
using IMDb.Domain;

namespace IMDb.API.Helpers
{
    /// <summary> Classe para mapeamento</summary>
    public class AutoMapperProfile : Profile
    {
        /// <summary> Mapeamento dominio x dto</summary>
        public AutoMapperProfile()
        {
            CreateMap<User, UserDto>().ReverseMap();
            CreateMap<Movie, MovieDto>().ReverseMap();
            CreateMap<User, UserLoginDto>().ReverseMap();
            CreateMap<Actor, ActorDto>().ReverseMap();
            CreateMap<MovieRating, MovieRatingDto>().ReverseMap();
        }
    }
}
